# Article supplementary materials
This repository contains [R scripts](script/) with statistical analyses performed for the purpose of the following article:

Loboda T.D., Guerra, J., Hosseini, R., & Brusilovsky P. (2014) Mastery Grids: An open source social educational progress visualization.  _European Conference on Technology Enhanced Learning (EC-TEL)_, 235-248. [[author's copy](ectel-14-loboda.pdf)]

For more information on the visualization software described in the article, see [this repository](https://gitlab.com/ubiq-x/mastery-grids).


## License
This project is licensed under the [BSD License](LICENSE.md).
